---
layout: home
---

# Quick Start

```bash
$ helm repo add zro https://zro.github.io/charts
```

## Current Charts

> <List of Charts>
{% for chart in site.data.index.entries %}
  <h3>{{ chart[0] }} (
    {% for properties in chart %}
      {{ properties[0].version }}
    {% endfor %}
  )</h3>
{% endfor %}

---
