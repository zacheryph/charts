.PHONY: refresh

refresh:
	find . -maxdepth 1 -type d -not -name '.*' -not -name docs -not -name template | \
		while read -r pkg ; do \
		  helm package -d docs $$pkg ; \
		done && \
	helm repo index --url "https://zro.github.io/charts" docs && \
	cp docs/index.yaml docs/_data/ && \
	git add docs
