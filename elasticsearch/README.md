# Elastic Search

Fluentd / ElasticSearch / Kibana.

This was mostly copied from k8s addons:

https://github.com/kubernetes/kubernetes/tree/master/cluster/addons/fluentd-elasticsearch

## DISABLED

My local "cluster" only has 16GB of memory and falls over when attempting to
run this. I have not done any actual testing other than knowing that it does
run and pushes the load of the server too 100+ :) For these reasons it is
disabled and will not produce chart packages.
