---
# Elasticsearch deployment itself
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: elasticsearch-logging
  labels:
    app: {{ template "pkg.name" . }}
    chart: {{ template "pkg.chart" . }}
    release: {{ .Release.Name }}
    heritage: {{ .Release.Service }}
    k8s-app: elasticsearch-logging
    version: v5.6.4
spec:
  serviceName: elasticsearch-logging
  replicas: 2
  selector:
    matchLabels:
      k8s-app: elasticsearch-logging
      version: v5.6.4
  volumeClaimTemplates:
  - metadata:
      name: data
    spec:
      storageClassName: manual
      accessModes:
      - ReadWriteOnce
      resources:
        requests:
          storage: {{ .Values.volumeSize }}
      selector:
        matchLabels:
          pvc-target: elasticsearch-data
  template:
    metadata:
      labels:
        k8s-app: elasticsearch-logging
        version: v5.6.4
    spec:
      serviceAccountName: elasticsearch-logging
      containers:
      - image: k8s.gcr.io/elasticsearch:v5.6.4
        name: elasticsearch-logging
        resources:
          # need more cpu upon initialization, therefore burstable class
          limits:
            cpu: 1000m
          requests:
            cpu: 100m
        ports:
        - containerPort: 9200
          name: db
          protocol: TCP
        - containerPort: 9300
          name: transport
          protocol: TCP
        volumeMounts:
        - name: data
          mountPath: /data
        env:
        - name: "NAMESPACE"
          valueFrom:
            fieldRef:
              fieldPath: metadata.namespace
      # elasticsearch requires vm.max_map_count to be at least 262144.
      # if your os already sets up this number to a higher value, feel free
      # to remove this init container.
      initContainers:
      - image: alpine:3.6
        command: ["/sbin/sysctl", "-w", "vm.max_map_count=262144"]
        name: elasticsearch-logging-init
        securityContext:
          privileged: true
