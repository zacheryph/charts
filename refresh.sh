#!/bin/sh
# Chart refresh
# this exists cause helm has no way to prevent
# overwriting an already existent package and i
# hate seeing the timestamps and possibly
# contents of chart versions changing

build_chart() {
  chart=$(basename "$1")
  echo "== building $chart"
  helm package -d .pkg "$1" >/dev/null
}

[ -f "./.env" ] && . ./.env
if [ -z "${CHARTS_URL}" ]; then
  echo "!! CHARTS_URL must be set"
  exit
fi

# make sure we have a clean slate
rm -rf .pkg
mkdir -p .pkg

# very specific chart given
if [ -n "$1" ]; then
  if [ ! -d "$1" ]; then
    echo "!! $1 chart not found."
    exit
  fi

  build_chart "$1"
else
  find . -maxdepth 2 -type f -name Chart.yaml -not -wholename ./template/Chart.yaml | \
  while read -r pkg ; do
    chart=$(dirname "$pkg")

    # only build if disabled file does not exist
    [ ! -e "${chart}/DISABLED" ] && build_chart "$chart"
  done
fi

# now prune any versions that already exist
find .pkg -type f -name \*.tgz | \
while read -r pkg ; do
  pkgfile=$(basename $pkg)
  if [ -f "docs/$pkgfile" ]; then
    # only warn if contents differ
    d1=$(openssl dgst -r -sha1 ".pkg/$pkgfile" | awk '{print $1}')
    d2=$(openssl dgst -r -sha1 "docs/$pkgfile" | awk '{print $1}')
    if [ "$d1" != "$d2" ]; then
      echo "** attempted to alter $pkgfile."
    fi
    rm ".pkg/$pkgfile"
  fi
done

# make new chart index
if ls .pkg/*.tgz >/dev/null 2>&1 ; then
  helm repo index --url "$CHARTS_URL" --merge "docs/index.yaml" .pkg
  mv .pkg/* docs/
  cp -f docs/index.yaml docs/_data/index.yaml
else
  echo "== no charts updated."
fi
